<div ng-controller="MenuController as mCtrl" ng-init="mCtrl.curtab = 'pos'">
<div ng-controller="PosController as pCtrl" ng-show="mCtrl.curtab  == 'pos'">
<div class="grid-container" >
   <div class="product">
      <div ng-include = "'view/template/product_section.template.php'"></div>
    </div>
    <div class="orderdetails">
   <div ng-include = "'view/template/order_details.template.php'"></div>
   </div>
</div>
<div ng-include = "'view/template/make_order_receipt.template.php'"></div>
<div ng-include = "'view/template/customer_form.template.php'"></div>
<div ng-include = "'view/template/modifier_popup.template.php'"></div>

</div>
<div ng-include = "'view/layout/orders.php'"  ng-show="mCtrl.curtab  == 'orders'"></div>
<div ng-include = "'view/layout/riders.php'" ng-show="mCtrl.curtab  == 'riders'"></div>
<div ng-include = "'view/layout/customers.php'" ng-show="mCtrl.curtab  == 'customers'"></div>

<div class="menu-bar">
<div class="bottom-menu">
   <ul class="foot-menu-items">
      <li><a id='pos' class=" active" ng-click="mCtrl.switchTab('pos','orders')">POS</a>
      </li>
       <li><a id='orders'  ng-click="mCtrl.switchTab('orders','pos')">Orders</a>
     <!--  <li><a ng-click="mCtrl.redirect('/orders.php')">Orders</a> -->
      </li>
   </ul>
</div>

<div ng-include = "'view/template/close_register.template.php'"></div>
<div ng-include = "'view/template/side_menu.template.php'"></div>
<div class="foot-hamburger-container">
   <span ng-click="mCtrl.openMenu()" uk-icon="icon: menu; ratio: 1.5" class="uk-icon">
   </span>
</div>
</div>
</div>
